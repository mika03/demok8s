package com.example.demok8s.controllers;

import com.example.demok8s.DTOs.StudentDTO;
import com.example.demok8s.DTOs.StudentInfoDTO;
import com.example.demok8s.services.StudentService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/student", produces = "application/json")
@CrossOrigin("*")
@Tag(name = "Student API")
public class StudentController {
    @Autowired
    StudentService studentService;

//    @RequestMapping(value = "/add", method = RequestMethod.POST, consumes = "application/json")
//    @Operation(summary = "add")
//    private void add(@RequestBody StudentDTO dto) {
//        studentService.add(dto);
//    }

//    @RequestMapping(value = "/list", method = RequestMethod.GET)
//    @Operation(summary = "list")
//    private List<StudentInfoDTO> list() {
//        return studentService.list();
//    }

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    @Operation(summary = "Hello")
    private String list() {
        return "Hello friends";
    }
}

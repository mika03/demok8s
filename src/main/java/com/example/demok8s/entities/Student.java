//package com.example.demok8s.entities;
//
//import lombok.AllArgsConstructor;
//import lombok.Getter;
//import lombok.NoArgsConstructor;
//import lombok.Setter;
//import org.bson.types.ObjectId;
//import org.springframework.data.annotation.Id;
//import org.springframework.data.mongodb.core.mapping.Document;
//
//@Document(collection = "Student")
//@Getter
//@Setter
//@NoArgsConstructor
//@AllArgsConstructor
//public class Student {
//    @Id
//    private ObjectId id;
//    private String name;
//}
